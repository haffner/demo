sheet: download trim html

download:
	wget --no-check-certificate -O export.csv "https://docs.google.com/spreadsheets/d/1946GwGOfdTc0Q5KYBXcKGcMIMDH3SOwmpUBFQJbNO3E/export?gid=0&format=csv"

trim:
	cp export.csv tmp.csv
	tail +2 tmp.csv > export.csv
	rm tmp.csv	

html:
	pandoc -M document-css=false -H header.html -A footer.html --wrap=preserve -r csv --metadata title="J2 LEO & A Timeline" export.csv -o export.html

clean:
	rm export.{csv,html}
