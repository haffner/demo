# Download a Google sheet as CSV and convert it to an html table

- Skip to step 2 if you already have a CSV file.

**Required:**
- `css.html` and `sheet.css` in this repository.
- pandoc

**Steps:**

### 1. Download the Google sheet as a CSV file.

You will need a link to the sheet, and to then replace `edit#gid=0` with `export?gid=0&format=csv` in that link.

Ex. change this,

```
https://docs.google.com/spreadsheets/d/1946GwGOfdTc0Q5KYBXcKGcMIMDH3SOwmpUBFQJbNO3E/edit#gid=0
```

to this:

```
https://docs.google.com/spreadsheets/d/1946GwGOfdTc0Q5KYBXcKGcMIMDH3SOwmpUBFQJbNO3E/export?gid=0&format=csv
```

Then run wget with this link to download the Google sheet as a CSV file.

```
wget --no-check-certificate -O export.csv "https://docs.google.com/spreadsheets/d/1946GwGOfdTc0Q5KYBXcKGcMIMDH3SOwmpUBFQJbNO3E/export?gid=0&format=csv"
```


### 2. Convert the CSV file to an html table.

```
pandoc -M document-css=false -H header.html -A footer.html --wrap=preserve -r csv --metadata title="J2 LEO & A Timeline" export.csv -o export.html
```
